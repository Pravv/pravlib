#ifndef ALGORITHM_H
#define ALGORITHM_H

#include "constructor.h"
#include "meta.h"
#include <algorithm>
#include <iterator>
#include <vector>

namespace prv {

template <typename Cont, typename It = typename Cont::const_iterator>
class Range {
    It b, e;

public:
    using iterator = It;
    using UnderType = std::decay_t<Cont>;

    Range(const Cont& cont)
        : b(std::begin(cont))
        , e(std::end(cont))
    {
    }

    Range(It b, It e)
        : b(b)
        , e(e)
    {
    }

    It begin() const { return b; }
    It end() const { return e; }
};

template <typename T>
auto makeRange(T&& t)
{
    if constexpr (prv::range_traits<std::decay_t<T>>::value == true) {
        return t;
    } else if constexpr (prv::range_traits<std::decay_t<T>>::value == false) {
        return prv::Range<std::decay_t<T>>{ t };
    }
}

template <typename ORange, typename It = decltype(std::rbegin(std::declval<ORange>()))>
auto reverse(ORange&& originalRange)
{
    return Range<std::decay_t<ORange>>(std::rbegin(originalRange), std::rend(originalRange));
}

template <typename ORange, typename It = decltype(std::begin(std::declval<ORange>()))>
auto from(ORange&& originalRange, std::size_t n)
{
    return Range<std::decay_t<ORange>>(std::begin(originalRange) + n, std::end(originalRange));
}

template <typename ORange, typename It = decltype(std::begin(std::declval<ORange>()))>
auto skipLines(ORange&& originalRange, int lineCount)
{
    auto newStart = std::begin(originalRange);
    while (lineCount-- > 0) {
        newStart = std::find(newStart, std::end(originalRange), '\n') + 1;
    }
    return Range<std::decay_t<ORange>>(newStart, std::end(originalRange));
}
};
#endif // ALGORITHM_H
