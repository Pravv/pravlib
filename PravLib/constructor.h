#pragma once

#define DEFAULT_MOVE_CONSTR(className, deleteORdefault) \
    className(className&&) = deleteORdefault;           \
    className& operator=(className&&) = deleteORdefault;

#define DEFAULT_COPY_CONSTR(className, deleteORdefault) \
    className(const className&) = deleteORdefault;      \
    className& operator=(const className&) = deleteORdefault;
