#ifndef UTILS_H
#define UTILS_H

#include <iterator>
#include <type_traits>

namespace details {
template <class,
    class = void>
struct range_traits_base { // empty for non-range
    static constexpr bool value = false;
};

template <class _Iter>
struct range_traits_base<_Iter,
    std::void_t<typename _Iter::UnderType>> {
    static constexpr bool value = true;

    using UnderType = typename _Iter::UnderType;
};
}

namespace prv {
template <typename E>
constexpr auto toUnderlying(E e) noexcept
{
    return static_cast<std::underlying_type_t<E>>(e);
}

template <typename... T>
struct dependent_false {
    static constexpr bool value = false;
};
//https://dev.krzaq.cc/post/checking-whether-a-class-has-a-member-function-with-a-given-signature/
//https://jguegant.github.io/blogs/tech/sfinae-introduction.html

template <typename T>
struct has_method {
private:
    using yes = std::true_type;
    using no = std::false_type;

    template <typename U>
    static auto testSubstr(int) -> decltype(std::declval<U>().substr(1, 1), yes());

    template <typename U>
    static auto testFind(int) -> decltype(std::declval<U>().find(std::declval<U>()), yes());

    template <typename>
    static no testSubstr(...);
    template <typename>
    static no testFind(...);

public:
    static constexpr bool substr = std::is_same_v<decltype(testSubstr<T>(0)), yes>;
    static constexpr bool find = std::is_same_v<decltype(testFind<T>(0)), yes>;
};

template <typename UnnamedType>
struct is_valid_impl {
private:
    using yes = std::true_type;
    using no = std::false_type;

    template <typename... Params>
    constexpr auto test_validity(int) -> decltype(std::declval<UnnamedType>()(std::declval<Params>()...), yes{})
    {
        return yes{};
    }

    template <typename... Params>
    constexpr no test_validity(...) { return no{}; }

public:
    template <typename... Params>
    constexpr auto operator()(Params&&...)
    {
        return test_validity<Params...>(0);
    }
};

template <typename UnnamedType>
constexpr auto is_valid(UnnamedType&&)
{
    return is_valid_impl<UnnamedType>();
}

auto has_substr_v = prv::is_valid([](auto&& x) -> decltype(x.substr(1, 1)) {});

// clang-format off
template <class _Iter> struct range_traits : details::range_traits_base<_Iter> {};
// clang-format on
};

#endif // UTILS_H
