#pragma once

#include "details.h"
#include <algorithm>
#include <string>
#include <vector>

namespace prv {
template <typename T, typename _ = void>
struct is_container : std::false_type {
};

// clang-format off
template <typename T>
struct is_container<T, typename std::conditional<false,
                                                 details::is_container_helper<
                                                     typename T::value_type,
                                                     typename T::size_type,
                                                     typename T::iterator,
                                                     typename T::const_iterator>,
                                                 void
                                                >::type
                    > : public std::true_type
{
};
// clang-format on

template <typename ReturnT,
    typename IterT,
    typename SeparatorT>
ReturnT join(IterT start, IterT end, const SeparatorT& separator)
{
    auto itBegin = start;
    auto itEnd = end;

    ReturnT result;
    result.reserve(static_cast<typename ReturnT::size_type>(std::distance(itBegin, itEnd)));

    if (itBegin != itEnd) {
        result += *itBegin;
        ++itBegin;
    }

    for (; itBegin != itEnd; ++itBegin) {
        result += separator;
        result += *itBegin;
    }

    return result;
}

template <typename ReturnT,
    typename IterT,
    typename SeparatorT,
    typename ElementConventer>
ReturnT join(IterT start, IterT end, const SeparatorT& separator, ElementConventer elemConv)
{
    auto itBegin = start;
    auto itEnd = end;

    ReturnT result;
    result.reserve(static_cast<typename ReturnT::size_type>(std::distance(itBegin, itEnd)));

    if (itBegin != itEnd) {
        result += elemConv(*itBegin);
        ++itBegin;
    }

    for (; itBegin != itEnd; ++itBegin) {
        result += separator;
        result += elemConv(*itBegin);
    }

    return result;
}

template <typename ReturnT = std::string,
    typename ContainerT,
    typename SeparatorT>
ReturnT join(const ContainerT& input, const SeparatorT& separator)
{
    static_assert(is_container<ContainerT>::value, "prv::join() works only with stl-like containers!");
    return join<ReturnT>(std::begin(input), std::end(input), separator);
}

template <typename ReturnT = std::string,
    typename ContainerT,
    typename SeparatorT,
    typename ElementConventer>
ReturnT join(const ContainerT& input, const SeparatorT& separator, ElementConventer elemConv)
{
    static_assert(is_container<ContainerT>::value, "prv::join() works only with stl-like containers!");
    return join<ReturnT>(std::begin(input), std::end(input), separator, elemConv);
}
};
