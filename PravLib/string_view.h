#ifndef STRING_VIEW_H
#define STRING_VIEW_H
#include <string>
namespace prv
{
//NOTE: not sure if its the right design
class string_view
{
public:
    string_view(const string_view& rso) = default;
    string_view(const char* pointer, size_t length)
        : pointer(std::move(pointer))
        , length(std::move(length))
    {
    }

    string_view(const char* pointer)
        : pointer(std::move(pointer))
        , length(std::char_traits<char>::length(pointer))
    {
    }

    string_view(const char* pointer, int length)
        : pointer(std::move(pointer))
        , length(length)
    {
    }

    string_view(const std::string& rso)
        : pointer(std::move(rso.data()))
        , length(std::move(rso.length()))
    {
    }

    string_view& operator=(const std::string& rso)
    {
        pointer = rso.data();
        length  = rso.length();
        return *this;
    }

    string_view& operator=(const string_view& rso) = default;

    inline std::string to_string() const
    {
        return std::string(pointer, length);
    }

    const char* data() { return pointer; }
    size_t size() { return length; }
private:
    const char* pointer;
    size_t length;
};
};
#endif // STRING_VIEW_H
