#ifndef UNIQUE_H
#define UNIQUE_H

#include <memory>
#include <cstdint>
template <class T>
class CustomDeleter
{
public:
    void operator()(T* pX)
    {
        auto x = std::default_delete<T>(pX);
    }
};

template <class T>
class CustomDeleter_2
{
public:
    void operator()(T pX)
    {
        auto x = std::default_delete<T>(pX);
    }
};
template <class T, class D = CustomDeleter_2<T>>
using CustomDeleterPtr_2 = std::unique_ptr<T, D>;

template <class T, class D = CustomDeleter<T>>
using CustomDeleterPtr = std::unique_ptr<T, D>;

template <class T, class D = CustomDeleter<T>>
auto MakeUnique(T* x) -> std::unique_ptr<T, D>
{
    return std::unique_ptr<T, D>(x);
}

template <class T, class D = CustomDeleter_2<T>>
auto MakeUnique_2(T* x) -> std::unique_ptr<T, D>
{
    return std::unique_ptr<T, D>(x);
}
#define CUSTOM_DELETER_SMART_POINTER_2(SomeType, SomeTypeDeleter) \
    template <>                                                   \
    class CustomDeleter_2<SomeType>                               \
    {                                                             \
    public:                                                       \
        void operator()(SomeType pX)                              \
        {                                                         \
            SomeTypeDeleter(pX);                                  \
        }                                                         \
    };                                                            \
                                                                  \
    using SomeType##_UniquePtr = CustomDeleterPtr_2<SomeType>;

#define CUSTOM_DELETER_SMART_POINTER(SomeType, SomeTypeDeleter) \
    template <>                                                 \
    class CustomDeleter<SomeType>                               \
    {                                                           \
    public:                                                     \
        void operator()(SomeType* pX)                           \
        {                                                       \
            SomeTypeDeleter(pX);                                \
        }                                                       \
    };                                                          \
                                                                \
    using SomeType##_UniquePtr = CustomDeleterPtr<SomeType>;

#define DEFINE_SMART_POINTER(SomeType, SomeTypeDeleter) \
    class C##SomeType##Deleter                          \
    {                                                   \
    public:                                             \
        void operator()(SomeType* pX)                   \
        {                                               \
            SomeTypeDeleter(pX);                        \
        }                                               \
    };                                                  \
    using SomeType##_UniquePtr = std::unique_ptr<SomeType, C##SomeType##Deleter>

#define DEFINE_SMART_POINTER_ARRAY(SomeType, SomeTypeDeleter) \
    class C##SomeType##Deleter                                \
    {                                                         \
    public:                                                   \
        void operator()(SomeType* pX)                         \
        {                                                     \
            SomeTypeDeleter(pX);                              \
        }                                                     \
    };                                                        \
    using SomeType##_UniquePtr = std::unique_ptr<SomeType[], C##SomeType##Deleter>

namespace
{
template <typename T>
class CRefHelper final
{
public:
    using element_type = typename T::element_type;

    ~CRefHelper()
    {
        m_smartPointer.reset(m_rawPointer);
    }

    CRefHelper(T& smarPointer)
        : m_smartPointer(smarPointer)
        , m_rawPointer(smarPointer.get())
    {
    }

    operator element_type**()
    {
        return &m_rawPointer;
    }
    operator element_type*&()
    {
        return m_rawPointer;
    }

private:
    void* operator new(std::size_t count) = delete;

private:
    T& m_smartPointer;
    element_type* m_rawPointer;
};
}

namespace prv
{
template <typename T>
auto Ref(T& smartPointer) -> CRefHelper<T>
{
    return CRefHelper<T>(smartPointer);
}
}
#endif // UNIQUE_H
