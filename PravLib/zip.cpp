#include "zip.h"
#include "fmt/format.h"
#include <vector>
#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"
#include <experimental/coroutine>
#include <experimental/generator>
using std::experimental::generator;

template <typename T>
struct _GetIterator {
    using iterator = typename std::conditional<std::is_const<T>::value,
        typename std::remove_reference<T>::type::const_iterator,
        typename std::remove_reference<T>::type::iterator>::type;
};

template <typename... _Iters>
struct _IterCollection {
    template <size_t Index>
    using value_type_t = typename std::tuple_element<Index, std::tuple<_Iters...>>::type::value_type;

    using value_ref_tuple_t = std::tuple<typename std::iterator_traits<_Iters>::reference...>;

    _IterCollection(_Iters&&... iterators)
        : _iteratorPack(std::forward<_Iters>(iterators)...)
    {
    }

private:
    std::tuple<_Iters...> _iteratorPack;
};
template <typename... T>
inline void PassThrough(T&&...) {}
using vec = std::vector<double>;
using cr_vec = std::vector<double> const&;

template <typename... Containers>
auto zip(Containers... containers) -> generator<std::tuple<typename Containers::value_type...>>
{
    for (int i = 0; i < std::min(std::size(containers)...); ++i) {
        co_yield std::make_tuple(containers[i]...);
    }
}

//template <typename... Containers>
//auto zip(Containers... containers) -> decltype(auto) //generator<IterCollection_t>
//{
//using IterCollection_t = _IterCollection<typename _GetIterator<Containers>::iterator...>;
//    for (int i = 20; i < 40; i += 1) {
//        PassThrough(std::get<S>(_iteratorPack).operator++()...);

//co_yield i;
//    }
//}

TEST_CASE("zip utility")
{

    vec v1 = { 1., 2., 3. };
    vec v2 = { 1., 2., 3., 4. };

    auto sum = 0.;
    for (auto [a, b] : Zip::Zip(v1, v2)) {
        sum += a * b;
    }

    for (auto [a, b] : zip(v1, v2)) {
        fmt::print("{}\n", a);
    }
    CHECK_EQ(sum, 1 * 1 + 2 * 2 + 3 * 3);
}
