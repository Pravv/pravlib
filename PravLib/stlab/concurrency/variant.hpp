/*
    Copyright 2015 Adobe
    Distributed under the Boost Software License, Version 1.0.
    (See accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
*/

/**************************************************************************************************/

#ifndef STLAB_CONCURRENCY_VARIANT_HPP
#define STLAB_CONCURRENCY_VARIANT_HPP

#include <PravLib/stlab/concurrency/config.hpp>

#include <variant>

namespace stlab {

template <typename... T>
using variant = std::variant<T...>;

template <class T, class... Types>
constexpr T& get(std::variant<Types...>& v)
{
    return std::get<T>(v);
}

template <class T, class... Types>
constexpr T&& get(std::variant<Types...>&& v)
{
    return std::get<T>(std::move(v));
}

template <class T, class... Types>
constexpr const T& get(const std::variant<Types...>& v)
{
    return std::get<T>(v);
}

template <class T, class... Types>
constexpr const T&& get(const std::variant<Types...>&& v)
{
    return std::get<T>(std::move(v));
}

template <typename... Types>
constexpr auto index(const std::variant<Types...>& v)
{
    return v.index();
}

} // stlab

#endif
