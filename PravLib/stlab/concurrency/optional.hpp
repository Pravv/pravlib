/*
    Copyright 2015 Adobe
    Distributed under the Boost Software License, Version 1.0.
    (See accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
*/

/**************************************************************************************************/

#ifndef STLAB_CONCURRENCY_OPTIONAL_HPP
#define STLAB_CONCURRENCY_OPTIONAL_HPP

#include <PravLib/stlab/concurrency/config.hpp>

#include <optional>

namespace stlab {

template <typename T>
using optional = std::optional<T>;

constexpr std::nullopt_t nullopt{ std::nullopt };

} // stlab

#endif
