#pragma once

#include <stdexcept>
namespace stlab {

enum class future_error_codes { // names for futures errors
    broken_promise = 1,
    reduction_failed,
    no_state,
    promise_already_satisfied,
    future_already_retrieved,

};

/**************************************************************************************************/
inline namespace v1 {
    namespace detail {

        inline const char* Future_error_map(future_error_codes code) noexcept
        { // convert to name of future error
            switch (code) { // switch on error code value
            case future_error_codes::broken_promise:
                return "broken promise";

            case future_error_codes::no_state:
                return "no state";

            case future_error_codes::reduction_failed:
                return "reduction failed";
                //NOTE:ADDED LATER TO STD::PROMISE
            case future_error_codes::future_already_retrieved:
                return ("future already retrieved");

            case future_error_codes::promise_already_satisfied:
                return ("promise already satisfied");

            default:
                return nullptr;
            }
        }

        /**************************************************************************************************/
    } // namespace v1
} // namespace detail

/**************************************************************************************************/

// future exception

class future_error : public std::logic_error {
public:
    explicit future_error(future_error_codes code)
        : logic_error("")
        , _code(code)
    {
    }

    const future_error_codes& code() const noexcept
    {
        return _code;
    }

    const char* what() const noexcept override
    {
        return detail::Future_error_map(_code);
    }

private:
    const future_error_codes _code; // the stored error code
};
}
