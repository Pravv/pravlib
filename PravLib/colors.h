#pragma once
#include "PravLib/fmt/format.h"
#include "PravLib/meta.h"
#include <string>

namespace prv {
enum class style {
    reset = 28,
    bold = 1,
    dim = 2,
    italic = 3,
    underline = 4,
    blink = 5,
    rblink = 6,
    reversed = 7,
    conceal = 8,
    crossed = 9
};
/*
enum class fg {
    black = 30,
    red = 31,
    green = 32,
    yellow = 33,
    blue = 34,
    magenta = 35,
    cyan = 36,
    gray = 37,
    reset = 39,
    supaReset = 0
};*/
enum class fg {
    black = 231,
    red = 1,
    green = 2,
    yellow = 3,
    blue = 32,
    magenta = 5,
    cyan = 6,
    gray = 8,
    white = 7,
    reset = 0
};
enum class bg {
    black = 40,
    red = 41,
    green = 42,
    yellow = 43,
    blue = 22,
    magenta = 45,
    cyan = 46,
    gray = 47,
    reset = 49
};
};

namespace prv {
template <typename T>
auto color22(T modif, const std::string& str) -> std::string
{
    if constexpr (std::is_enum_v<T>) {
        return fmt::format("\u001b[{};1m{}\u001b[{}m", prv::toUnderlying(modif), str, prv::toUnderlying(T::reset));
    } else {
        static_assert(prv::dependent_false<T>::value, "T must be enum class!");
    }
}

template <typename T, typename Printable>
auto color(T modif, Printable&& str) -> std::string
{
    if constexpr (std::is_enum_v<T>) {
        return fmt::format("\u001b[38;5;{}m{}\u001b[{}m", prv::toUnderlying(modif), str, prv::toUnderlying(T::reset));
    } else {
        static_assert(prv::dependent_false<T>::value, "T must be enum class!");
    }
}
} //namespace prv
