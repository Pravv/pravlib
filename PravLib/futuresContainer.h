#pragma once
#include <PravLib/stlab/concurrency/future.hpp>
#include <vector>

class tet {
    static std::vector<stlab::future<void>> futures;
    tet()
    {
        futures.push_back(stlab::async([&]() {
            for (auto&& future : futures) {
                future.get_try();
            }
        }));
    }
};
