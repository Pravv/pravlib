#pragma once
#include "algorithm.h"
#include "constructor.h"
#include "details.h"
#include "meta.h"
#include <algorithm>
#include <array>
#include <string>
#include <string_view>
#include <utility>
#include <vector>

namespace details {
struct not_function {
};
}
namespace prv {
/*
template <class ReturnT = std::string_view, template <class, class> class Container = std::vector, class ValueT, class PredicateT = details::not_function>
auto split(const ValueT& inputRange, std::string delimiter, [[maybe_unused]] PredicateT predicate = details::not_function{}) -> Container<ReturnT, std::allocator<ReturnT>>
{
    //auto range = prv::makeRange(inputRange);

    auto delimLenght = delimiter.size();

    std::vector<size_t> x;
    x.reserve(100);

    std::string::size_type start = 0;
    while ((start = inputRange.find(delimiter, start)) != std::string::npos) {
        x.push_back(start);
        start += delimLenght;
    }
    x.push_back(inputRange.size());

    Container<ReturnT, std::allocator<ReturnT>> out;
    out.reserve(std::size(x) + 1);

    std::size_t position = 0;
    for (size_t i = 0; i < std::size(x); ++i) {
        out.emplace_back(&inputRange.at(position), x[i] - position);
        position = x[i] + delimLenght;
    }

    return out;
}*/

template <class ReturnT = std::string_view, template <class, class> class Container = std::vector, class ValueT, class PredicateT = details::not_function>
auto split(const ValueT& inputRange, char delimiter, [[maybe_unused]] PredicateT predicate = details::not_function{}) -> Container<ReturnT, std::allocator<ReturnT>>
{
    auto range = prv::makeRange(inputRange);

    auto itBegin = std::begin(range);
    auto itEnd = std::end(range);

    const auto occurrences = std::count(itBegin, itEnd, delimiter);
    Container<ReturnT, std::allocator<ReturnT>> out;
    out.reserve(occurrences + 1);

    auto lastOccurence = itBegin;
    for (auto it = itBegin; it != itEnd; ++it) {
        if (*it == delimiter) {
            size_t dist{ (size_t)std::distance(lastOccurence, it) };
            if constexpr (std::is_same_v<details::not_function, PredicateT>) {
                out.emplace_back(&(*lastOccurence), dist);
            } else {
                out.emplace_back(std::move(predicate(std::string_view{ &(*lastOccurence), dist })));
            }

            lastOccurence = it + 1;
        }
    }

    size_t dist{ (size_t)std::distance(lastOccurence, itEnd) };
    if constexpr (std::is_same_v<details::not_function, PredicateT>) {
        out.emplace_back(&(*lastOccurence), dist);
    } else {
        out.emplace_back(std::move(predicate(std::string_view{ &(*lastOccurence), dist })));
    }

    return out;
}

template <size_t size, class ReturnT = std::string_view, class ValueT, class PredicateT = details::not_function>
auto split(const ValueT& inputRange, char delimiter, [[maybe_unused]] PredicateT predicate = details::not_function{}) -> std::array<ReturnT, size>
{
    auto range = prv::makeRange(inputRange);

    size_t maxNumberOfElements = 0;
    std::array<ReturnT, size> out;

    auto last_occurence = range.begin();
    for (auto it = range.begin(); it != range.end() && maxNumberOfElements < size - 1; ++it) {
        if (*it == delimiter) {
            size_t dist{ (size_t)std::distance(last_occurence, it) };
            if constexpr (std::is_same_v<details::not_function, PredicateT>) {
                out[maxNumberOfElements++] = ReturnT{ &(*last_occurence), dist };
            } else {
                out[maxNumberOfElements++] = std::move(predicate(std::string_view{ &(*last_occurence), dist }));
            }
            //out.emplace_back(&(*last_occurence), dist);
            last_occurence = it + 1;
        }
    }

    size_t dist{ (size_t)std::distance(last_occurence, range.end()) };
    if constexpr (std::is_same_v<details::not_function, PredicateT>) {
        out[maxNumberOfElements] = std::move(ReturnT{ &(*last_occurence), dist });
    } else {
        out[maxNumberOfElements] = std::move(predicate(std::string_view{ &(*last_occurence), dist }));
    }
    return out;
}
}
