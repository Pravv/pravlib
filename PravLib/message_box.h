#ifndef MESSAGE_BOX_H
#define MESSAGE_BOX_H
#include <QApplication>
#include <QIcon>
#include <QMessageBox>
namespace prv {
namespace messageBox {
    using cQString = const QString&;
    enum Icon {
        // keep this in sync with QMessageDialogOptions::Icon
        NoIcon = 0,
        Information = 1,
        Warning = 2,
        Critical = 3,
        Question = 4
    };

    inline void blocking(cQString message, Icon icon = Icon::NoIcon)
    {
        auto* msgBox = new QMessageBox();
        //msgBox->setWindowIcon(QIcon("C:\\Users\\Prav\\Desktop\\img\\9znh3c.png"));
        msgBox->setWindowModality(Qt::ApplicationModal);
        msgBox->setTextInteractionFlags(Qt::TextSelectableByMouse);
        msgBox->setAttribute(Qt::WA_DeleteOnClose);

        msgBox->setText(message);
        msgBox->setIcon((QMessageBox::Icon)icon);
        msgBox->show();
    }

    inline void alwaysOnTop(cQString windowTitle, cQString /*title*/, cQString message, Icon icon = Icon::NoIcon)
    {
        auto* msgBox = new QMessageBox();
        msgBox->setWindowFlags(msgBox->windowFlags() | Qt::WindowStaysOnTopHint | Qt::BypassWindowManagerHint);
        msgBox->setWindowModality(Qt::ApplicationModal);
        msgBox->setAttribute(Qt::WA_DeleteOnClose);
        msgBox->setTextInteractionFlags(Qt::TextSelectableByMouse);

        msgBox->setWindowTitle(windowTitle);
        // msgBox->setInformativeText(message);
        msgBox->setText(message);
        msgBox->setIcon((QMessageBox::Icon)icon);

        msgBox->show();
        // message = new QMessageBox ;
        //  message->setWindowFlags(message->windowFlags()|Qt::WindowStaysOnTopHint);
        ///  message->setWindowModality(Qt::ApplicationModal);
        //  message->setModal(true);
    }
}
}
#endif // MESSAGE_BOX_H
