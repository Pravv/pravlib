#ifndef BENCH_H
#define BENCH_H

#include <chrono>
#include <utility>

namespace prv
{
#define START_TIMER auto XDXDXDXDXD = std::chrono::high_resolution_clock::now();
#define STOP_TIMER std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - XDXDXDXDXD).count()

#define START_TIMER_N(_NAME) auto _NAME = std::chrono::high_resolution_clock::now();
#define STOP_TIMER_N(_NAME) std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - _NAME).count()

template <typename TimeUnit = std::chrono::milliseconds, typename Func, typename... Args>
auto duration(Func&& function, Args&&... args) -> TimeUnit
{
    auto start = std::chrono::steady_clock::now();
    std::forward<Func>(function)(std::forward<Args>(args)...);
    return std::chrono::duration_cast<TimeUnit>(std::chrono::steady_clock::now() - start);
}
};

#endif // BENCH_H
