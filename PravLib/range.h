#pragma once
namespace prv {

class range {
public:
    explicit range(int endValue)
        : start{ 0 }
        , endValue{ endValue }
    {
    }
    range(int start, int endValue)
        : start{ start }
        , endValue{ endValue }
    {
    }

    class RangeIterator {
    public:
        friend range;

        RangeIterator& operator++()
        {
            value += 1;
            return *this;
        }

        RangeIterator operator++(int)
        {
            value += 1;
            return RangeIterator(value - 1, end);
        }

        bool operator==(const RangeIterator& other) const
        {
            return (value == other.value && end == other.end) || (ended() && other.ended());
        }

        bool operator!=(const RangeIterator& other) const
        {
            return !(*this == other);
        }

        int operator*() const
        {
            return value;
        }

    private:
        RangeIterator(int end)
            : value{ end }
            , end{ end }
        {
        }
        RangeIterator(int value, int end)
            : value{ value }
            , end{ end }
        {
        }
        bool ended() const
        {
            return value >= end;
        }

        int value;
        int end;
    };

    RangeIterator begin() const
    {
        return RangeIterator(start, endValue);
    }
    RangeIterator end() const
    {
        return RangeIterator(endValue);
    }

private:
    int start;
    int endValue;
};
} //namespace prv
