#pragma once

#include <algorithm>
#include <cstdint>
#include <filesystem>
#include <fstream>
#include <string_view>
#include <tuple>
#include <utility>
#include <vector>
namespace fs = std::experimental::filesystem;

namespace details {
namespace {
    std::size_t getFileSize(std::ifstream& file)
    {
        file.seekg(0, std::ios_base::end);
        return static_cast<std::size_t>(file.tellg());
    }

    auto trimToLineCount(std::vector<char>::iterator start, std::vector<char>::iterator end, int lineCount, int newLineCount)
    {
        std::vector<char>::iterator newStart = start;
        while (newLineCount >= lineCount) {
            newStart = std::find(newStart, end, '\n') + 1;
            --newLineCount;
        }
        return newStart;
    }

    auto getLastLines(std::ifstream& file, std::size_t granularity, const int desiredLineCount) -> std::pair<std::vector<char>, int>
    {
        std::vector<char> buffer;
        int lineCount = 0;
        std::size_t fileSize = getFileSize(file);

        while (file && buffer.size() < fileSize && lineCount < desiredLineCount) {
            buffer.resize(std::min(buffer.size() + granularity, fileSize));

            file.seekg(-static_cast<std::streamoff>(buffer.size()), std::ios_base::end); //seek to position; from `end` to `end - buffer.size()`;

            file.read(buffer.data(), static_cast<long long>(buffer.size()));

            lineCount = static_cast<int>(std::count(buffer.begin(), buffer.end(), '\n'));
        }
        return { buffer, lineCount };
    }
}
};

namespace prv {
inline std::string tail(const fs::path& filename, int desiredLineCount)
{
    const std::size_t granularity = static_cast<size_t>(desiredLineCount);

    std::ifstream file(filename, std::ios_base::binary);

    auto [buffer, lineCount] = details::getLastLines(file, granularity, desiredLineCount);

    std::vector<char>::iterator start = details::trimToLineCount(buffer.begin(), buffer.end(), desiredLineCount, lineCount);

    std::vector<char>::iterator end = std::remove(start, buffer.end(), '\r'); //Windows Much

    return std::string(start, end);
}
};
/*
namespace details
{
namespace
{
    size_t getFileSize(std::ifstream& file)
    {
        file.seekg(0, std::ios_base::end);
        size_t fileSize = static_cast<size_t>(file.tellg());
        return fileSize;
    }



    auto getLastLines(std::ifstream& file, std::size_t granularity, const int lineCount) -> std::pair<std::vector<char>, int>
    {
        std::vector<char> buffer;
        int newlineCount     = 0;
        std::size_t fileSize = getFileSize(file);

        while (file && buffer.size() < fileSize && newlineCount < lineCount) {
            buffer.resize(std::min(buffer.size() + granularity, fileSize));

            file.seekg(-static_cast<std::streamoff>(buffer.size()), std::ios_base::end); //seek to position; from `end` to `end - buffer.size()`;

            file.read(buffer.data(), static_cast<long long>(buffer.size()));

            newlineCount = static_cast<int>(std::count(buffer.begin(), buffer.end(), '\n'));
        }
        return {buffer, newlineCount};
    }
}
};

namespace prv
{
inline std::string tail(std::string_view filename, int lineCount)
{
    const std::size_t granularity = static_cast<size_t>(lineCount);

    std::ifstream source(filename.data(), std::ios_base::binary);

    std::vector<char> buffer;
    int newlineCount = 0;
    std::tie(buffer, newlineCount) = details::getLastLines(source, granularity, lineCount);

    std::vector<char>::iterator start = details::trimToLineCount(buffer.begin(), buffer.end(), lineCount, newlineCount);

    std::vector<char>::iterator end = std::remove(start, buffer.end(), '\r');

    return std::string(start, end);
}
};
#endif // FILE_H
*/
