TEMPLATE = app
QT += core gui network widgets
CONFIG += c++17 console
QMAKE_CXXFLAGS += /std:c++latest /await

SOURCES +=     splitR.cpp \
    zip.cpp \
    Enum.cpp

HEADERS += pravlib.h \
            join.h \
            details.h \
            message_box.h \
            string_view.h \
    bench.h \
    algorithm.h \
    meta.h \
    file.h \
    constructor.h \
    unique.h \
    split.h \
    splitR.h \
    overload.h \
    common_types.h \
    ThreadPool2.h \
    fmt/container.h \
    fmt/format.h \
    fmt/ostream.h \
    fmt/posix.h \
    fmt/printf.h \
    fmt/string.h \
    fmt/time.h \
    stlab/concurrency/channel.hpp \
    stlab/concurrency/concurrency.hpp \
    stlab/concurrency/config.hpp \
    stlab/concurrency/default_executor.hpp \
    stlab/concurrency/executor_base.hpp \
    stlab/concurrency/future.hpp \
    stlab/concurrency/immediate_executor.hpp \
    stlab/concurrency/main_executor.hpp \
    stlab/concurrency/optional.hpp \
    stlab/concurrency/progress.hpp \
    stlab/concurrency/serial_queue.hpp \
    stlab/concurrency/system_timer.hpp \
    stlab/concurrency/task.hpp \
    stlab/concurrency/traits.hpp \
    stlab/concurrency/tuple_algorithm.hpp \
    stlab/concurrency/utility.hpp \
    stlab/concurrency/variant.hpp \
    stlab/copy_on_write.hpp \
    stlab/functional.hpp \
    stlab/memory.hpp \
    stlab/scope.hpp \
    stlab/utility.hpp \
    stlab/version.hpp \
    stlab/concurrency/test/shared_object.h \
    stlab/concurrency/test/promise.h \
    stlab/concurrency/test/future_status.h \
    nlohmann_json.h \
    range.h \
    futuresContainer.h \
    colors.h \
    zip.h \
    doctest.h \
    Enum.h

INCLUDEPATH += "D:\PROGRAMY\PravLib"

CONFIG(release, debug|release) {
    LIBS += D:\PROGRAMY\PravLib\libs\fmt.lib
    PRE_TARGETDEPS += D:\PROGRAMY\PravLib\libs\fmt.lib
}
CONFIG(debug, debug|release) {
    LIBS += D:\PROGRAMY\PravLib\libs\fmtd.lib
    PRE_TARGETDEPS+= D:\PROGRAMY\PravLib\libs\fmtd.lib
}
