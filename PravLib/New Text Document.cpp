#include "json.hpp" 
int main(){

   using json = nlohmann::json;
    using namespace std::string_literals;
	std::string jsonDoc = "test";
    try {
        auto doc = nlohmann::json::parse(jsonDoc);

        auto items = doc.at("items")[0];
        auto statistics = items.at("statistics");

        auto views = statistics.count("viewCount") ? std::stoi(statistics.at("viewCount").get<std::string>()) : 0;
        auto likes = statistics.count("likeCount") ? statistics.at("likeCount").get<std::string>() : "0"s;
        auto dislikes = statistics.count("dislikeCount") ? statistics.at("dislikeCount").get<std::string>() : "0"s;

        auto snippet = items.at("snippet");

        auto title = snippet.at("localized").at("title").get<std::string>();
    

        auto duration = items.at("contentDetails").at("duration").get<std::string>();

        

        return 0;
    } catch (...) {
        return 07;
}    }