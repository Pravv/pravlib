#pragma once

#include <utility>
#include <variant>

#define FWD(...) ::std::forward<decltype(__VA_ARGS__)>(__VA_ARGS__)
/*template <typename... F>
class overload : private F... {
public:
    overload(F... f)
        : F(f)...
    {
    }
    using F::operator()...;
};*/
namespace impl {
template <typename...>
struct overload_set;

template <typename TF>
struct overload_set<TF> : TF {
    using call_type = TF;
    using call_type::operator();

    overload_set(TF&& f) noexcept
        : TF(FWD(f))
    {
    }
};

template <typename TF, typename... TFs>
struct overload_set<TF, TFs...> : TF, overload_set<TFs...>::call_type {
    using base_type = typename overload_set<TFs...>::call_type;

    using f_type = TF;
    using call_type = overload_set;

    overload_set(TF&& f, TFs&&... fs) noexcept
        : f_type(FWD(f))
        , base_type(FWD(fs)...)
    {
    }

    using f_type::operator();
    using base_type::operator();
};
} //namespace imp

template <typename Obj, typename... TFs>
auto make_overload(Obj&& obj, TFs&&... fs) noexcept -> decltype(std::visit(impl::overload_set<TFs...>{ FWD(fs)... }, FWD(obj)))
{
    return std::visit(impl::overload_set<TFs...>{ FWD(fs)... }, FWD(obj));
}
